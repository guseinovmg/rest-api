'use strict';

const express = require('express');

const router = express.Router();

const db = require('../db.js');

router.post('/', (req, res) => {
    console.log('UserTracks post', req.body);
    db.UserTracks.create(req.body).then(result => {
        res.send(result);
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

router.get('/:id', (req, res) => {
    console.log('UserTracks get', req.params);
    db.UserTracks.findByPrimary(req.params.id).then(result => {
        res.send(result);
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

router.put('/', (req, res) => {
    console.log('UserTracks put', req.body);
    db.UserTracks.update(req.body, {
        where: {
            id: req.body.id
        },
        returning: true,
        raw: true
    }).then(result => {
        let usertrack = result[1][0];
        if (!usertrack) {
            res.status(404).send(`User id ${req.body.id} not found`);
        } else {
            res.send(usertrack);
        }
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

router.delete('/:id', (req, res) => {
    console.log('UserTracks delete', req.params);
    db.UserTracks.destroy({
        where: {
            id: req.params.id
        },
    }).then(result => {
        res.send({deleted: true});
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

module.exports = router;