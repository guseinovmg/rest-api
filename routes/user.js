'use strict';

const fs = require('fs');

const express = require('express');

const router = express.Router();

const db = require('../db.js');

const multer = require('multer');

const MAX_FILE_SIZE = 1024 * 1024 * 2;

let counter = 0;

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './static/uploads')
    },
    filename: function (req, file, cb) {
        console.log(file);
        const mime = file.mimetype;
        if (mime !== 'image/jpeg' && mime !== 'image/png') {
            cb('Can\'t accept mimetype ' + mime);
        }
        cb(null, Date.now().toString() + '-' + counter + '.' + mime.substr(6));
    }
});

const upload = multer({storage: storage});

function checkSize(req, res, next) {
    if (req.file) {
        if (req.file.size > MAX_FILE_SIZE) {
            res.status(500).send('Too big file size');
            return;
        }
        req.body.profile_photo = req.file.filename;
        next();
        return;
    }
    res.status(500).send('Photo must be set');
}

router.post('/', upload.single('profile_photo'), checkSize, (req, res) => {
    console.log('User post', req.body, req.file);
    db.Users.create(req.body).then(result => {
        res.send(result);
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

router.get('/:id', (req, res) => {
    console.log('User get', req.params);
    db.Users.findByPrimary(req.params.id).then(result => {
        res.send(result);
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

router.put('/', upload.single('profile_photo'), checkSize, (req, res) => {
    console.log('User put', req.body, req.file);
    let profile_photo;
    db.Users.findByPrimary(req.body.id).then(_result => {
        profile_photo = _result.profile_photo;
        return db.Users.update(req.body, {
            where: {
                id: req.body.id
            },
            returning: true,
            raw: true
        })
    }).then(result => {
        let user = result[1][0];
        if (!user) {
            res.status(404).send(`User id ${req.body.id} not found`);
        } else {
            res.send(user);
        }
        fs.unlink('./static/uploads/' + profile_photo, console.log);
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

router.delete('/:id', (req, res) => {
    console.log('User delete', req.params);
    let profile_photo;
    db.Users.findByPrimary(req.params.id).then(_result => {
        profile_photo = _result.profile_photo;
        return db.Users.destroy({
            where: {
                id: req.params.id
            },
        })
    }).then(_ => {
        fs.unlink('./static/uploads/' + profile_photo, console.log);
        res.send({deleted: true});
    }).catch(err => {
        res.status(500).send(err.message);
    });
});

module.exports = router;