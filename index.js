"use strict";

const express = require("express");

const app = express();

const body_parser = require('body-parser');

app.use(body_parser.json());

app.use(body_parser.urlencoded({extended: true}));

app.use(express.static('./static'));

app.use('/user', require('./routes/user.js'));

app.use('/usertrack', require('./routes/usertrack'));

app.listen(3333, function () {
    console.log('App listening on port 3333!');
});