'use strict';

const should = require('should');

const request = require('request-promise');

const fs = require('fs');

const URL = 'http://localhost:3333';

const USER_URL = URL + '/user';

const USERTRACK_URL = URL + '/usertrack';

describe('User', function () {
    let user_id = -1;
    describe('Create', function () {
        it('should return created id of user', function () {
            return request.post({
                uri: USER_URL,
                formData: {
                    name: 'murad',
                    email: 'guseinovmg@gmail.com',
                    profile_photo: fs.createReadStream('./pic.jpg')
                },
                transform: (body) => JSON.parse(body)
            }).then((response) => {
                console.log('Response:', response);
                response.should.have.property('id').which.is.a.Number();
                user_id = response.id;
            })
        });
    });
    describe('Read', function () {
        it('should return a user', function () {
            return request.get({
                uri: USER_URL + '/' + user_id,
                transform: (body) => JSON.parse(body)
            }).then((response) => {
                console.log('Response:', response);
                response.should.have.property('id').which.is.a.Number();
                response.should.have.property('name').which.is.a.String();
                response.should.have.property('email').which.is.a.String();
                response.should.have.property('profile_photo').which.is.a.String();
            })
        });
    });
    describe('Update', function () {
        it('should return updated user', function () {
            return request.put({
                uri: USER_URL,
                formData: {
                    id:user_id,
                    name: 'murad',
                    email: 'guseinovmg@gmail.com',
                    profile_photo: fs.createReadStream('./pic.jpg')
                },
                transform: (body) => JSON.parse(body)
            }).then((response) => {
                console.log('Response:', response);
                response.should.have.property('id').which.is.a.Number();
                response.should.have.property('name').which.is.a.String();
                response.should.have.property('email').which.is.a.String();
                response.should.have.property('profile_photo').which.is.a.String();
            })
        });
    });
    describe('Delete', function () {
        it('should return {deleted:true}', function () {
            return request.delete({
                uri: USER_URL + '/' + user_id,
                transform: (body) => JSON.parse(body)
            }).then((response) => {
                console.log('Response:', response);
                response.should.have.property('deleted').which.is.a.Boolean();
            })
        });
    });
});

describe('UserTrack', function () {
    let user_id = -1;

    let usertrack_id = -1;

    before(function () {
        return request.post({
            uri: USER_URL,
            formData: {
                name: 'murad',
                email: 'guseinovmg@gmail.com',
                profile_photo: fs.createReadStream('./pic.jpg')
            },
            transform: (body) => JSON.parse(body)
        }).then((response) => {
            console.log('Response:', response);
            response.should.have.property('id').which.is.a.Number();
            user_id = response.id;
            console.log(user_id);
        });
    });

    after(function () {
        return request.delete({
            uri: USER_URL + '/' + user_id,
            transform: (body) => JSON.parse(body)
        }).then((response) => {
            console.log('Response:', response);
            response.should.have.property('deleted').which.is.a.Boolean();
        })
    });

    describe('Create', function () {
        it('should return created id of usertrack', function () {
            return request.post({
                uri: USERTRACK_URL,
                body: {
                    user_id: user_id,
                    track_url: 'https://soundcloud.com/penguin-books/the-penguin-podcast-jane-corry-with-paul-smith'
                },
                json: true
            }).then((response) => {
                console.log('Response:', response);
                response.should.have.property('id').which.is.a.Number();
                usertrack_id = response.id;
            })
        });
    });
    describe('Read', function () {
        it('should return a usertrack', function () {
            return request.get({
                uri: USERTRACK_URL + '/' + usertrack_id,
                transform: (body) => JSON.parse(body)
            }).then((response) => {
                console.log('Response:', response);
                response.should.have.property('id').which.is.a.Number();
                response.should.have.property('user_id').which.is.a.Number();
                response.should.have.property('track_url').which.is.a.String();
            })
        });
    });
    describe('Update', function () {
        it('should return updated usertrack', function () {
            return request.put({
                uri: USERTRACK_URL,
                body: {
                    id: usertrack_id,
                    user_id: user_id,
                    track_url: 'https://soundcloud.com/penguin-books/the-penguin-podcast-jane-corry-with-paul-smith'
                },
                json: true
            }).then((response) => {
                console.log('Response:', response);
                response.should.have.property('id').which.is.a.Number();
                response.should.have.property('user_id').which.is.a.Number();
                response.should.have.property('track_url').which.is.a.String();
            })
        });
    });
    describe('Delete', function () {
        it('should return {deleted:true}', function () {
            return request.delete({
                uri: USERTRACK_URL + '/' + usertrack_id,
                transform: (body) => JSON.parse(body)
            }).then((response) => {
                console.log('Response:', response);
                response.should.have.property('deleted').which.is.a.Boolean();
            })
        });
    });
});