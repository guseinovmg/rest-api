'use strict';

const Sequelize = require('sequelize');

const sequelize = new Sequelize('dbname', 'postgres', '777888999', {
    host: 'localhost',
    port: 5432,
    dialect: 'postgres',

    pool: {
        max: 5,
        min: 0,
        idle: 10000
    }
});

const db = {};

db.Users = sequelize.define('users', {
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            isEmail: true
        }
    },
    profile_photo: {
        type: Sequelize.STRING,
        allowNull: false
    }
}, {
    freezeTableName: true,
    underscored: true,
    timestamps: false
});

db.UserTracks = sequelize.define('usertracks', {
    user_id: {
        type: Sequelize.INTEGER,
        references: {
            model: db.Users,
            key: 'id'
        }
    },
    track_url: {
        type: Sequelize.STRING,
        validate: {
            isUrl: true,
            contains: 'https://soundcloud.com/'
        }
    }
}, {
    freezeTableName: true,
    underscored: true,
    timestamps: false
});

db.sequelize = sequelize;

Object.freeze(db);

module.exports = db;